package com.jedi.trabalholayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_linear_layout)
    protected Button buttonLinearlayout;

    @BindView(R.id.button_grid_layout)
    protected Button buttonGridlayout;

    @BindView(R.id.button_relative_layout)
    protected Button buttonRelativeLayout;

    @BindView(R.id.button_table_layout)
    protected Button buttonTableayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_linear_layout)
    protected void buttonLinearLayoutClick(View view){
        Intent intent = new Intent(this, LinearActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button_grid_layout)
    protected void buttonGridLayoutClick(View view){
        Intent intent = new Intent(this, GridActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button_relative_layout)
    protected void buttonRelativeLayoutClick(View view){
        Intent intent = new Intent(this, RelativeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.button_table_layout)
    protected void buttonTableLayoutClick(View view){
        Intent intent = new Intent(this, TableActivity.class);
        startActivity(intent);
    }
}
